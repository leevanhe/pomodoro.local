New Media Design & Development III
==================================

Informatie
-------------

- Van Hecke Lee
- Opleidingsonderdeel: NMDAD
- Academiejaar: 2016-17
- Opleiding: Bachelor in de grafische en digitale media
- Afstudeerrichting: Multimediaproductie
- Keuzeoptie: proDEV
- Opleidingsinstelling: Arteveldehogeschool

```
Pomodoro.local/
├── ionic/
├── symfony/
├── docs/
└── README.md
```

Benodigdheden
-------------

[Artevelde Laravel Homestead][]

Installatie
-----------

### Project installeren

```
$ cd Code
$ git clone https://gitlab.com/leevanhe/pomodoro.local.git
$ cd pomodoro.local
$ artestead make --type symfony --ip 192.168.10.55
$ composer update
```

### Server starten

```
$ cd pomodoro.local
$ vu
```

### Backoffice en API (Symfony App)

```
$ cd pomodoro.local
$ vss
vagrant@pomodoro$ cd symfony
vagrant@pomodoro$ composer update
vagrant@pomodoro$ console artevelde:database:user
vagrant@pomodoro$ console artevelde:database:init --seed --migrate
vagrant@pomodoro$ exit
$ _
```

### Frontoffice (Ionic App)

```
$ cd pomodoro.local
$ cd ionic
$ yarn install
$ ionic serve --lab
```

URI's
-----

 - <http://www.pomodoro.local>

[Artevelde Laravel Homestead]: http://www.gdm.gent/artestead/