<?php

namespace ApiBundle\Controller;


use AppBundle\Entity\Pomodoro;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PomodoroController extends Controller
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsPomodoroAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'GET');

        return $response;
    }

    /**
     * @param $pomodoro_id
     *
     * @return mixed
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getPomodoroAction($pomodoro_id)
    {
        $em = $this->getDoctrine()->getManager();
        $pomodoro = $em
            ->getRepository(Pomodoro::class)
            ->find($pomodoro_id);

        if (!$pomodoro instanceof Pomodoro) {
            throw new NotFoundHttpException('Not found');
        }

        return $pomodoro;
    }

    /**
     * @return mixed
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getPomodorosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $pomodoros = $em
            ->getRepository(Pomodoro::class)
            ->findAll();

        return $pomodoros;
    }
}
