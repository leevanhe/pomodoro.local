<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\FeedbackType;
use AppBundle\Entity\feedback;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FeedbackController.
 */
class FeedbackController extends FOSRestController
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsFeedbackAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * @param $feedback_id
     *
     * @return mixed
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getFeedbackAction($feedback_id)
    {
        $em = $this->getDoctrine()->getManager();
        $feedback = $em
            ->getRepository(feedback::class)
            ->find($feedback_id);

        if (!$feedback instanceof Pomodoro) {
            throw new NotFoundHttpException('Not found');
        }

        return $feedback;
    }

    /**
     * @return mixed
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getFeedbacksAction()
    {
        $em = $this->getDoctrine()->getManager();
        $feedbacks = $em
            ->getRepository(feedback::class)
            ->findAll();

        return $feedbacks;
    }

    /**
     * Post a new feedback of a user.
     *
     * { "feedback": { "title": "lorem", "body": "lorem ipsum" } }
     *
     * @param Request $request
     * @param $user_id
     *
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post(
     *     "/users/{user_id}/feedback/",
     *     requirements = {
     *         "user_id": "\d+"
     *     }
     * )
     *
     * @Nelmio\ApiDoc(
     *     input = ApiBundle\Form\FeedbackType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postFeedbackAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em
            ->getRepository(User::class)
            ->find($user_id);
        if (!$user instanceof User) {
            throw new NotFoundHttpException();
        }

        $feedback = new feedback();
        $feedback->setUser($user);

        $logger = $this->get('logger');
        $logger->info($request);

        return $this->processFeedbackForm($request, $feedback);
    }

    // Convenience methods
    // -------------------

    /**
     * Process FeedbackType Form.
     *
     * @param Request $request
     * @param feedback $feedback
     * @return View|Response
     */
    private function processFeedbackForm(Request $request, feedback $feedback)
    {
        $form = $this->createForm(FeedbackType::class, $feedback, ['method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($feedback->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            $em = $this->getDoctrine()->getManager();
            $em->persist($feedback); // Manage entity Article for persistence.
            $em->flush();           // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('api_v1_get_user_feedback', [
                    'user_id' => $feedback->getUser()->getId(),
                    'feedback_id' => $feedback->getId(),
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                'feedback' => ['id' => $feedback->getId()],
            ]));

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
