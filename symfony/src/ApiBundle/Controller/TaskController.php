<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Task;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskController extends Controller
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsTaskAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * @param $task_id
     *
     * @return mixed
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getTaskAction($task_id)
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em
            ->getRepository(Task::class)
            ->find($task_id);

        if (!$task instanceof Task) {
            throw new NotFoundHttpException('Not found');
        }

        return $task;
    }

    /**
     * @return mixed
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getTasksAction()
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em
            ->getRepository(Task::class)
            ->findAll();

        return $task;
    }

    /**
     * Post a new task.
     *
     * { "article": { "name": "Lorem", "description": "ipsum" } }
     *
     * @param Request $request
     * @return View|Response
     *
     * @FOSRest\View()
     * @FOSRest\Post(
     *     "/tasks/"
     * )
     *
     * @Nelmio\ApiDoc(
     *     input = TaskType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postTaskAction(Request $request)
    {

        $task = new Task();

        $logger = $this->get('logger');
        $logger->info($request);

        return $this->processTaskForm($request, $task);
    }

    /**
     * Update an article for a user.
     *
     * @param Request $request
     * @param int $task_id
     *
     * @return Response
     *
     * @FOSRest\View()
     * @FOSRest\Put(
     *     requirements = {
     *         "task_id" : "\d+",
     *         "_format" : "json|xml"
     *     }
     * )
     *
     * @Nelmio\ApiDoc(
     *     input = TaskType::class,
     *     statusCodes = {
     *         Response::HTTP_NO_CONTENT: "No Content"
     *     }
     * )
     */
    public function putTaskAction(Request $request, $task_id)
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em
            ->getRepository(Task::class)
            ->find($task_id);

        if (!$task instanceof Task) {
            throw new NotFoundHttpException();
        }

        if ($task->getId() === (int) $task_id) {
            return $this->processTaskForm($request, $article);
        }
    }
}
