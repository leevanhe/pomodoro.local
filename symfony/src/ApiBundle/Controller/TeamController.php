<?php

namespace ApiBundle\Controller;


use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TeamController extends Controller
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsTeamAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'GET');

        return $response;
    }

    /**
     * @return mixed
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getTeamsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $teams = $em
            ->getRepository(Team::class)
            ->findAll();

        return $teams;
    }

    /**
     *
     * @param $user_id
     *
     * @return mixed
     *
     * @FOSRest\Get(
     *     "/users/{user_id}/teams",
     *     requirements = {
     *          "user_id" : "\d+",
     *         "_format" : "json|jsonp|xml"
     *     }
     * )
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK : "OK"
     *     }
     * )
     */
    public function getTeamsByUserAction($user_id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em
            ->getRepository(User::class)
            ->find($user_id);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Not found');
        }

        $teams = $user->getMembers();

        return $teams;
    }

    /**
     * @return mixed
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getTeamAction($team_id)
    {
        $em = $this->getDoctrine()->getManager();
        $team = $em
            ->getRepository(Team::class)
            ->find($team_id);

        if (!$team instanceof Team) {
            throw new NotFoundHttpException('Not found');
        }

        return $team;
    }
}
