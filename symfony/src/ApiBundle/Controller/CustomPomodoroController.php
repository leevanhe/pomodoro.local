<?php

namespace ApiBundle\Controller;

use ApiBundle\Form\CustomPomodoroType;
use AppBundle\Entity\CustomPomodoros;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController as Controller;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\View\View;


class CustomPomodoroController extends Controller
{
    /**
     * Test API options and requirements.
     *
     * @return Response
     *
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function optionsCustomPomodoroAction()
    {
        $response = new Response();
        $response->headers->set('Allow', 'OPTIONS, GET, POST, PUT');

        return $response;
    }

    /**
     * @return mixed
     * @Nelmio\ApiDoc(
     *     resource = true,
     *     statusCodes = {
     *         Response::HTTP_OK: "OK"
     *     }
     * )
     */
    public function getCustomPomodorosAction()
    {
        $em = $this->getDoctrine()->getManager();
        $custompomodoros = $em
            ->getRepository(CustomPomodoros::class)
            ->findAll();
        return $custompomodoros;
    }

    /**
     * Post a new pomodoro of a user.
     *
     * @param Request $request
     * @param int $user_id
     * @return View|Response
     * @Nelmio\ApiDoc(
     *     input = ArticleType::class,
     *     statusCodes = {
     *         Response::HTTP_CREATED : "Created"
     *     }
     * )
     */
    public function postCustomPomodoroAction(Request $request, $user_id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em
            ->getRepository(User::class)
            ->find($user_id);
        if (!$user instanceof User) {
            throw new NotFoundHttpException();
        }

        $custompomodoro = new CustomPomodoros();
        $custompomodoro->setUser($user);

        $logger = $this->get('logger');
        $logger->info($request);

        return $this->processCustomPomodoroForm($request, $custompomodoro);
    }

    // Convenience methods
    // -------------------

    /**
     * Process ArticleType Form.
     *
     * @param Request $request
     * @param CustomPomodoros $CustomPomodoro
     * @return View|Response
     */
    private function processCustomPomodoroForm(Request $request, CustomPomodoros $CustomPomodoro)
    {
        $form = $this->createForm(CustomPomodoroType::class, $CustomPomodoro, ['method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $statusCode = is_null($CustomPomodoro->getId()) ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

            $em = $this->getDoctrine()->getManager();
            $em->persist($CustomPomodoro); // Manage entity Article for persistence.
            $em->flush();           // Persist to database.

            $response = new Response();
            $response->setStatusCode($statusCode);

            // Redirect to the URI of the resource.
            $response->headers->set('Location',
                $this->generateUrl('api_v1_get_user_custompomodoro', [
                    'user_id' => $CustomPomodoro->getUser()->getId(),
                    'custompomodoros_id' => $CustomPomodoro->getId(),
                ], /* absolute path = */true)
            );
            $response->setContent(json_encode([
                'custompomodoros' => ['id' => $CustomPomodoro->getId()],
            ]));

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }

}
