<?php

namespace ApiBundle\Form;

use AppBundle\Entity\CustomPomodoros;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ImageType
 * @package ApiBundle\Form
 */
class CustomPomodoroType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', null, ['required' => false ])
            ->add('name')
            ->add('time')
            ->add('shortbreak')
            ->ad('longbreak');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\CustomPomodoros',
            'csrf_protection' => false,
        ));
    }

    /**
     * JSON object name.
     *
     * { article: { … } }
     *
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'custompomodoros';
    }
}
