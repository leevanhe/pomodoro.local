<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Socialmedia;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class LoadSocialmediaData.
 *
 * @author Lee Van Hecke <leevanhe@student.arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadSocialmediaData extends AbstractFixture implements OrderedFixtureInterface
{
    const COUNT = 5;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 4; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $social = new Socialmedia();
        $em->persist($social); // Manage Entity for persistence.
        $social->setFacebook('https://www.facebook.com/');
        $social->setLinkedin('https://www.linkedin.com/');
        $social->setTwitter('https://twitter.com/');
        $social->setInstagram('https://www.instagram.com/');

        $social->setUser($this->getReference("LeeVanHecke"));

        $em->flush();
    }
}
