<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

/**
 * Class LoadTeamData.
 *
 * @author Lee Van Hecke <leevanhe@student.arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadTeamData extends AbstractFixture implements OrderedFixtureInterface
{
    const COUNT = 5;

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $team = new Team();
        $em->persist($team);
        $team->setName('GoTeam');

        $this->addReference("testteam", $team);

        for ($teamCount = 0; $teamCount < self::COUNT; ++$teamCount) {
            $team = new Team();
            $em->persist($team);
            $team->setName($faker->colorName());

            $this->addReference("testteam-${teamCount}", $team);
        }

        $em->flush();
    }
}
