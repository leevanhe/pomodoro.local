<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Pomodoro;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadPomodoroData.
 *
 * @author Lee Van Hecke <leevanhe@student.arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */
class LoadPomodoroData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 2; // The order in which fixture(s) will be loaded.
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';

        $pomodoro = new Pomodoro();
        $em->persist($pomodoro); // Manage Entity for persistence.
        $pomodoro
            ->setTime(1500)
            ->setShortBreak(300)
            ->setLongBreak(900);


        $em->flush(); // Persist all managed Entities.
    }
}
