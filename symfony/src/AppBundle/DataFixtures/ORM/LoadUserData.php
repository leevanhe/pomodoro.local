<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use AppBundle\Traits\ContainerTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadUserData.
 *
 * @author Lee Van Hecke <leevanhe@student.arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 */

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerTrait;

    const COUNT = 2;

    public function getOrder()
    {
        return 3;
    }

    public function load(ObjectManager $em)
    {
        $encoder = $this->container->get('security.password_encoder');

        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $user = new User();
        $em->persist($user);
        $user->setFirstName('Lee');
        $user->setLastName('Van Hecke');
        $user->setUsername('leevanhe');
        $user->setUsernameCanonical('leevanhe');
        $user->setEmail('leevanhe@student.arteveldehs.be');
        $user->setEmailCanonical('leevanhe@student.arteveldehs.be');
        $user->setRoles(array('ROLE_ADMIN'));
        $user->setUri('http://www.arteveldehogeschool.be/images/artev_e-signature_rgb_s.png');
        $user->setWork('Front-end developer');

        $plainPassword = 'n64MJ6H2';
        $encodedPassword = $encoder->encodePassword($user, $plainPassword);
        $user->setPassword($encodedPassword);

        $user->setTeam($this->getReference("testteam"));

        $this->addReference("LeeVanHecke", $user);

        $user = new User();
        $em->persist($user);
        $user->setFirstName('Kaj');
        $user->setLastName('De Borger');
        $user->setUsername('kajdeb');
        $user->setUsernameCanonical('kajdeb');
        $user->setEmail('kajdeb@student.arteveldehs.be');
        $user->setEmailCanonical('kajdeb@student.arteveldehs.be');
        $user->setRoles(array('ROLE_ADMIN'));
        $user->setUri('http://www.arteveldehogeschool.be/images/artev_e-signature_rgb_s.png');
        $user->setWork('Manager');

        $plainPassword = 'testtest';
        $encodedPassword = $encoder->encodePassword($user, $plainPassword);
        $user->setPassword($encodedPassword);

        $user->setTeam($this->getReference("testteam"));

        $this->addReference("KajDeBorger", $user);


        for($teamCount = 0; $teamCount < LoadTeamData::COUNT; ++$teamCount) {
            for ($userCount = 0; $userCount < self::COUNT; ++$userCount) {
                $user = new User();
                $em->persist($user);
                $user->setFirstName($faker->firstName());
                $user->setLastName($faker->lastName());
                $user->setUsername($faker->userName());
                $user->setUsernameCanonical($faker->userName());
                $user->setEmail($faker->email());
                $user->setEmailCanonical($faker->email());
                $user->setRoles(array('ROLE_USER'));
                $user->setUri($faker->imageUrl());
                $user->setWork($faker->jobTitle());

                $user->setPassword($encoder->encodePassword($user, '1234'));

                $user->setTeam($this->getReference("testteam-${teamCount}"));
            }
        }

        $em->flush();
    }
}
