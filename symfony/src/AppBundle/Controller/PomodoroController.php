<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PomodoroController extends Controller
{
    /**
     * @Route("/pomodoro")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Pomodoro:index.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/pomodoro_create")
     */
    public function createAction()
    {
        return $this->render('AppBundle:Pomodoro:create.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/pomodoro_new")
     */
    public function newAction()
    {
        return $this->render('AppBundle:Pomodoro:new.html.twig', array(
            // ...
        ));
    }

}
