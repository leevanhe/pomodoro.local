<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Task
 *
 *
 *  @author Lee Van Hecke <leevanhe@student.arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
 */
class Task
{
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    // Member Variables for Relationships.
    // -----------------------------------

    /**
     * @ORM\OneToOne(targetEntity="CustomPomodoros", inversedBy="task")
     * @ORM\JoinColumn(name="custompomodoros_id", referencedColumnName="id")
     */
    private $custompomodoros;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Task
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Task
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    // Member Methods for Relationships.
    // ---------------------------------

    /**
     * Get CustomPomodoros.
     *
     * @return Pomodoro
     */
    public function getCustomPomodoros()
    {
        return $this->custompomodoros;
    }

    /**
     * Set CustomPomodoros.
     *
     * @param CustomPomodoros $custompomodoros
     *
     * @return Task
     */
    public function setCustomPomodoros(CustomPomodoros $custompomodoros)
    {
        $this->custompomodoros = $custompomodoros;

        return $this;
    }
}

