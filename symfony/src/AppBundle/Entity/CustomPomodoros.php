<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CustomPomodoros
 *
 * @ORM\Table(name="custompomodoros")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomPomodorosRepository")
 */
class CustomPomodoros extends Pomodoro
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    // Member Variables for Relationships.
    // -----------------------------------

    /**
     * @ORM\OneToOne(targetEntity="Task", mappedBy="custompomodoros")
     */
    private $task;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="custompomodoros")
     */
    private $users;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    // Member Methods for Relationships.
    // ---------------------------------

    /**
     * Get task.
     *
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set task.
     *
     * @param Task $task
     *
     * @return CustomPomodoros
     */
    public function setTask(Task $task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get users.
     *
     * @return Collection
     */
    public function getUsers()
    {
        return $this->posts;
    }

    /**
     * Set users.
     *
     * @param ArrayCollection $users
     * @return CustomPomodoros
     */
    public function setUsers(ArrayCollection $users)
    {
        $this->users = $users;

        return $this;
    }


}

