<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity Pomodoro
 *
 * @author Lee Van Hecke <leevanhe@student.arteveldehs.be>
 * @copyright Copyright © 2016-2017, Artevelde University College Ghent
 *
 * @ORM\Table(name="pomodoros")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PomodoroRepository")
 */
class Pomodoro
{
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", options={"unsigned":true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \int
     *
     * @ORM\Column(name="time", type="integer")
     */
    private $time;

    /**
     * @var \int
     *
     * @ORM\Column(name="short_break", type="integer")
     */
    private $shortBreak;

    /**
     * @var \int
     *
     * @ORM\Column(name="long_break", type="integer")
     */
    private $longBreak;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Pomodoro
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set time
     *
     * @param \int $time
     *
     * @return Pomodoro
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set shortBreak
     *
     * @param \int $shortBreak
     *
     * @return Pomodoro
     */
    public function setShortBreak($shortBreak)
    {
        $this->shortBreak = $shortBreak;

        return $this;
    }

    /**
     * Get shortBreak
     *
     * @return \int
     */
    public function getShortBreak()
    {
        return $this->shortBreak;
    }

    /**
     * Set longBreak
     *
     * @param \int $longBreak
     *
     * @return Pomodoro
     */
    public function setLongBreak($longBreak)
    {
        $this->longBreak = $longBreak;

        return $this;
    }

    /**
     * Get longBreak
     *
     * @return \int
     */
    public function getLongBreak()
    {
        return $this->longBreak;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pomodoro
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pomodoro
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function PomodoroTime()
    {
        $seconds = $this->getTime();
        return gmdate('H:i:s', $seconds).' min';
    }


    public function PomodoroShortbreak()
    {
        $seconds = $this->getShortBreak();
        return gmdate('H:i:s', $seconds).' min';
    }

    public function PomodoroLongbreak()
    {
        $seconds = $this->getLongBreak();
        return gmdate('H:i:s', $seconds).' min';
    }
}

