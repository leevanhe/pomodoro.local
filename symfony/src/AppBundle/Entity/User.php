<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Serializable;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser implements Serializable
{
    public function __toString()
    {
        return (string) $this->getId();
    }

    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(name="id",type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     * @JMS\SerializedName("firstName")
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     * @JMS\SerializedName("lastName")
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="work", type="string", length=255)
     *
     * @JMS\SerializedName("work")
     */
    private $work;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255, nullable=true)
     *
     */
    protected $uri;

    /**
     * @var UploadedFile
     *
     * http://api.symfony.com/2.7/Symfony/Component/HttpFoundation/File/UploadedFile.html
     *
     * @Assert\NotBlank(groups={"Backoffice"})
     * @Assert\File(maxSize="6000000", groups={"Backoffice"})
     * @Assert\File(mimeTypes={"image/gif", "image/png", "image/jpeg"}, groups={"Backoffice"})
     */
    protected $file;


    // Member Variables for Relationships.
    // -----------------------------------

    /**
     * @var Team
     *
     *
     * Many-to-one relationship with Team inversed by Team::$users.
     * Further reading: http://docs.doctrine-project.org/en/latest/reference/association-mapping.html#one-to-many-bidirectional
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="users")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id", nullable=true)
     */
    protected $team;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="CustomPomodoros", inversedBy="users")
     * @ORM\JoinTable(name="users_has_custompomodoros",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="custompomodoros_id", referencedColumnName="id")}
     * )
     */
    protected $custompomodoros;

    /**
     * One User has One Socialmedia.
     * @ORM\OneToOne(targetEntity="Socialmedia", mappedBy="user")
     */
    private $socialmedia;

    /**
     * @ORM\OneToMany(targetEntity="feedback", mappedBy="user")
     */
    private $feedback;

    /**
     * @ORM\OneToMany(targetEntity="Rating", mappedBy="user")
     */
    private $rating;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setEnabled(true);
        $this->setFeedback(new ArrayCollection());
        $this->setRating(new ArrayCollection());
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set work
     *
     * @param string $work
     *
     * @return User
     */
    public function setWork($work)
    {
        $this->work = $work;

        return $this;
    }

    /**
     * Get work
     *
     * @return string
     */
    public function getWork()
    {
        return $this->work;
    }


    /**
     * Set uri.
     *
     * @param string $uri
     *
     * @return User
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri.
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    // Member Methods for Relationships.
    // ---------------------------------

    /**
     * Set team.
     *
     * @param $team
     *
     * @return User
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team.
     *
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Get CustomPomodoros.
     *
     * @return ArrayCollection
     */
    public function getCustomPomodoros()
    {
        return $this->custompomodoros;
    }

    /**
     * Set cat.
     *
     * @param ArrayCollection $custompomodoros
     *
     * @return User
     */
    public function setPomodoros(ArrayCollection $custompomodoros)
    {
        $this->custompomodoros = $custompomodoros;

        return $this;
    }


    /**
     * Set socialmedia
     *
     * @param ArrayCollection $psocialmedia
     *
     * @return User
     */
    public function setSocialmedia ($socialmedia)
    {
        $this->socialmedia = $socialmedia;
        return $this;
    }

    /**
     * Get socialmedia
     *
     * @return ArrayCollection
     */
    public function getSocialmedia()
    {
        return $this->socialmedia;
    }

    /**
     * @return ArrayCollection
     */
    public function getFeedback()
    {
        return $this->feedback;
    }

    /**
     * @param ArrayCollection $posts
     *
     * @return User
     */
    public function setFeedback (ArrayCollection $feedback)
    {
        $this->feedback = $feedback;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param ArrayCollection $posts
     *
     * @return User
     */
    public function setRating (ArrayCollection $rating)
    {
        $this->rating = $rating;
        return $this;
    }

    // Methods for Serializable implementation.
    // ----------------------------------------
    // http://php.net/manual/en/class.serializable.php

    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
        ]);
    }

    /**
     * @see \Serializable::unserialize()
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password) = unserialize($serialized);
    }
}
