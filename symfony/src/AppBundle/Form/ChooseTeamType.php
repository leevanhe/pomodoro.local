<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class ChooseCourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teams', EntityType::class, array(
                'class' => 'AppBundle:Team',
                'choice_label' => 'name',
            ));
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'team_choice';
    }
}