<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PomodoroControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/pomodoro');
    }

    public function testCreate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/pomodoro_create');
    }

    public function testNew()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/pomodoro_new');
    }

}
