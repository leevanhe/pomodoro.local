import { Component } from '@angular/core';

import { AuthService } from '../../providers/pomodoro-auth';

/*
  Generated class for the AccountDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-account-detail',
  templateUrl: 'account-detail.html'
})
export class AccountDetailPage {

  user: any;

  constructor(public userdata: AuthService) {
    this.user = this.userdata.fetchUser();
  }
}
