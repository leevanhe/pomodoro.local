import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Rate page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-rate',
  templateUrl: 'rate.html'
})
export class RatePage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello RatePage Page');
  }

}
