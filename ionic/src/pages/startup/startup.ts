import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalController, NavController } from 'ionic-angular';

import { HomePage }  from '../home/home';
import { AuthService } from '../../providers/pomodoro-auth';
import { ICredentials } from '../../models/ICredentials';

@Component({
  selector: 'page-startup',
  templateUrl: 'startup.html'
})

export class StartupPage {

  constructor(public modalCtrl: ModalController) {}

  LoginModal() {
    let LoginModal = this.modalCtrl.create(LoginPage);
    LoginModal.present();
  }

}

@Component({
  templateUrl: 'login.html'
})

export class LoginPage {

  login: ICredentials = <ICredentials>{};
  submitted = false;

  constructor(public navCtrl: NavController,
              public userdata: AuthService) {
  }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userdata.login(this.login);
      console.log('Form is valid.');
      this.navCtrl.push(HomePage);
    } else {
      console.log('Form is not valid.');
      this.submitted = false;
    }
  }
}