import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomePage } from '../home/home'
import { Feedback } from '../../models/feedback';
import { PomodoroFeedback } from '../../providers/pomodoro-feedback';

@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html'
})
export class FeedbackPage {

  private submitted: boolean = false;
  feedback: Feedback = {};

  constructor(public navCtrl: NavController,
              private pomodoroFeedback: PomodoroFeedback) {}

  ionViewDidLoad() {
    console.log('Hello FeedbackPage Page');
  }

  onSubmit(form) {
    this.submitted = true;

    if (form.valid) {
      console.log('Form is valid.');
      console.log('POST feedback: ' + JSON.stringify(this.feedback));
      this.pomodoroFeedback
          .postFeedback(this.feedback)
          .subscribe();
      this.navCtrl.push(HomePage);
    } else {
      console.log('Form is not valid.');
      this.submitted = false;
    }
  }
}
