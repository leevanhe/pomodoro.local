import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomePage }  from '../home/home';
import { SharePage }  from '../share/share';
import { GraphsPage } from '../graphs/graphs';
import { TeammembersPage } from '../teammembers/teammembers';
import { LeaderboardsPage } from '../leaderboards/leaderboards';
import { AccountPage } from '../account/account';
import { SoundPage } from '../sound/sound';
import { RatePage } from '../rate/rate';
import { FeedbackPage } from '../feedback/feedback';

/*
  Generated class for the Options page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-options',
  templateUrl: 'options.html'
})
export class OptionsPage {

  constructor(public navCtrl: NavController) {}

  goToHomePage() {
    this.navCtrl.push(HomePage);
  }

  goToSharePage() {
    this.navCtrl.push(SharePage);
  }

  goToGraphsPage() {
    this.navCtrl.push(GraphsPage);
  }

  goToTeammembersPage() {
    this.navCtrl.push(TeammembersPage);
  }

  goToLeaderboardsPage() {
    this.navCtrl.push(LeaderboardsPage);
  }

  goToAccountPage() {
    this.navCtrl.push(AccountPage);
  }

  goToSoundPage() {
    this.navCtrl.push(SoundPage);
  }

  goToRatePage() {
    this.navCtrl.push(RatePage);
  }

  goToFeedbackPage() {
    this.navCtrl.push(FeedbackPage);
  }

}
