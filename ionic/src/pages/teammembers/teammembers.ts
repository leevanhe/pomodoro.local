import { Component } from '@angular/core';
import { ModalController, NavParams , ViewController, NavController } from 'ionic-angular';

import { HomePage }  from '../home/home';
import { SharePage }  from '../share/share';

import { GraphsPage } from '../graphs/graphs';
import { LeaderboardsPage } from '../leaderboards/leaderboards';
import { AccountPage } from '../account/account';
import { OptionsPage } from '../options/options';

@Component({
  selector: 'page-teammembers',
  templateUrl: 'teammembers.html'
})
export class TeammembersPage {

  constructor(
      public navCtrl: NavController,
      public modalCtrl: ModalController
  ) {}

  //Navigation

  goToHomePage() {
    this.navCtrl.push(HomePage);
  }

  goToSharePage() {
    this.navCtrl.push(SharePage);
  }

  goToGraphsPage() {
    this.navCtrl.push(GraphsPage);
  }

  goToLeaderboardsPage() {
    this.navCtrl.push(LeaderboardsPage);
  }

  goToAccountPage() {
    this.navCtrl.push(AccountPage);
  }

  goToOptionsPage() {
    this.navCtrl.push(OptionsPage);
  }

  //Modal

  DetailModal() {
    let DetailModal = this.modalCtrl.create(DetailPage);
    DetailModal.present();
  }

}

@Component({
  templateUrl: 'detail.html'
})

export class DetailPage {

  constructor(
      public params: NavParams,
      public viewCtrl: ViewController,
      public navCtrl: NavController
  ) {}

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
