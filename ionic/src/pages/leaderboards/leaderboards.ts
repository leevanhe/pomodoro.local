import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomePage }  from '../home/home';
import { SharePage }  from '../share/share';
import { GraphsPage } from '../graphs/graphs';
import { TeammembersPage } from '../teammembers/teammembers';
import { AccountPage } from '../account/account';
import { OptionsPage } from '../options/options';
/*
  Generated class for the Leaderboards page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-leaderboards',
  templateUrl: 'leaderboards.html'
})
export class LeaderboardsPage {

  leaderboards: any;

  constructor(public navCtrl: NavController) {

    this.leaderboards = [
        'Lee Van Hecke ',
        'Amelie Vanhove',
        'Eddy Vanhove',
        'Kaj De Borger',
        'Bianca Van De Vijver',
        'Kurt Van Hecke',
        'Kurt De Borger',
    ]
  }

  //Navigation

  goToHomePage() {
    this.navCtrl.push(HomePage);
  }

  goToSharePage() {
    this.navCtrl.push(SharePage);
  }

  goToGraphsPage() {
    this.navCtrl.push(GraphsPage);
  }

  goToTeammembersPage() {
    this.navCtrl.push(TeammembersPage);
  }

  goToAccountPage() {
    this.navCtrl.push(AccountPage);
  }

  goToOptionsPage() {
    this.navCtrl.push(OptionsPage);
  }

}
