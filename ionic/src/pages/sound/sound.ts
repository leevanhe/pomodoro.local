import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Sound page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-sound',
  templateUrl: 'sound.html'
})
export class SoundPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello SoundPage Page');
  }

}
