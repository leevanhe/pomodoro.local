import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage }  from '../startup/startup';
import { HomePage }  from '../home/home';
import { SharePage }  from '../share/share';
import { GraphsPage } from '../graphs/graphs';
import { TeammembersPage } from '../teammembers/teammembers';
import { LeaderboardsPage } from '../leaderboards/leaderboards';
import { AccountDetailPage } from '../account-detail/account-detail';
import { OptionsPage } from '../options/options';

import { AuthService } from '../../providers/pomodoro-auth';


/*
  Generated class for the Account page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  user: any;

  constructor(public navCtrl: NavController,
              public userdata: AuthService) {
    this.user = this.userdata.fetchUser();
    console.log(this.user);
  }

  goToHomePage() {
    this.navCtrl.push(HomePage);
  }

  goToSharePage() {
    this.navCtrl.push(SharePage);
  }

  goToGraphsPage() {
    this.navCtrl.push(GraphsPage);
  }

  goToTeammembersPage() {
    this.navCtrl.push(TeammembersPage);
  }

  goToLeaderboardsPage() {
    this.navCtrl.push(LeaderboardsPage);
  }

  goToAccountDetailPage() {
    this.navCtrl.push(AccountDetailPage);
  }

  goToOptionsPage() {
    this.navCtrl.push(OptionsPage);
  }

  logout() {
    this.userdata.logout();
    this.navCtrl.setRoot(LoginPage);
  }

}
