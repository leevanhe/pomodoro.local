import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomePage }  from '../home/home';
import { SharePage }  from '../share/share';
import { TeammembersPage } from '../teammembers/teammembers';
import { LeaderboardsPage } from '../leaderboards/leaderboards';
import { AccountPage } from '../account/account';
import { OptionsPage } from '../options/options';

/*
  Generated class for the Graphs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-graphs',
  templateUrl: 'graphs.html'
})
export class GraphsPage {

  constructor(public navCtrl: NavController) {}

  goToHomePage() {
    this.navCtrl.push(HomePage);
  }

  goToSharePage() {
    this.navCtrl.push(SharePage);
  }

  goToTeammembersPage() {
    this.navCtrl.push(TeammembersPage);
  }

  goToLeaderboardsPage() {
    this.navCtrl.push(LeaderboardsPage);
  }

  goToAccountPage() {
    this.navCtrl.push(AccountPage);
  }

  goToOptionsPage() {
    this.navCtrl.push(OptionsPage);
  }

}
