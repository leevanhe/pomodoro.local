import {Component, Input} from '@angular/core';
import {ITimer} from './itimer';

import { NavController, ModalController, NavParams, ViewController } from 'ionic-angular';
import { SharePage }  from '../share/share';
import { HomePage} from './home';
import { Task } from '../../models/task';
import { PomodoroTask } from '../../providers/pomodoro-task';



@Component({
    selector: 'timer',
    templateUrl: 'timer.html'
})
export class TimerComponent {

    @Input() timeInSeconds: number;
    public timer: ITimer;

    constructor(public navCtrl: NavController,
                public modalCtrl: ModalController) {}

    PomodoroOptionsModal() {
        let PomodoroOptionsModal = this.modalCtrl.create(PomodoroOptionsPage);
        PomodoroOptionsModal.present();
    }

    goToSharePage() {
        this.navCtrl.push(SharePage);
    }


    ngOnInit() {
        this.initTimer();
    }

    hasFinished() {
        return this.timer.hasFinished;
    }

    initTimer() {
        if(!this.timeInSeconds) { this.timeInSeconds = 0; }

        this.timer = <ITimer>{
            seconds: this.timeInSeconds,
            runTimer: false,
            hasStarted: false,
            hasFinished: false,
            secondsRemaining: this.timeInSeconds
        };

        this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
    }

    startTimer() {
        this.timer.hasStarted = true;
        this.timer.runTimer = true;
        this.timerTick();
    }

    pauseTimer() {
        this.timer.runTimer = false;
    }

    resumeTimer() {
        this.startTimer();
    }

    timerTick() {
        setTimeout(() => {
            if (!this.timer.runTimer) { return; }
            this.timer.secondsRemaining--;
            this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
            if (this.timer.secondsRemaining > 0) {
                this.timerTick();
            }
            else {
                this.timer.hasFinished = true;
            }
        }, 1000);
    }

    getSecondsAsDigitalClock(inputSeconds: number) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return hoursString + ':' + minutesString + ':' + secondsString;
    }
}

@Component({
    templateUrl: 'options.html'
})

export class PomodoroOptionsPage {

    time: number = 25;
    longbreak: number = 15;
    shortbreak: number = 5;

    private submitted: boolean = false;

    task: Task = {};

    constructor(
        public params: NavParams,
        public viewCtrl: ViewController,
        public navCtrl: NavController,
        private pomodorotask: PomodoroTask
    ) {}

    goToHomePage() {
        this.navCtrl.push(HomePage);
    }

    onSubmit(form) {
        this.submitted = true;

        if (form.valid) {
            console.log('Form is valid.');
            if (this.task.hasOwnProperty('id')) {
                console.log('PUT task: ', JSON.stringify(this.task));
                this.pomodorotask
                    .putTask(this.task)
                    .subscribe();
            } else {
                console.log('POST article: ' + JSON.stringify(this.task));
                this.pomodorotask
                    .postTask(this.task)
                    .subscribe();
            }
            this.navCtrl.push(HomePage);
        } else {
            console.log('Form is not valid.');
            this.submitted = false;
        }
    }
}
