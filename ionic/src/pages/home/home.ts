import { Component, ViewChild } from '@angular/core';

import { NavController, LoadingController} from 'ionic-angular';
import { GraphsPage } from '../graphs/graphs';
import { TeammembersPage } from '../teammembers/teammembers';
import { LeaderboardsPage } from '../leaderboards/leaderboards';
import { AccountPage } from '../account/account';
import { OptionsPage } from '../options/options';

import { Pomodoro } from '../../models/pomodoro';
import {  PomodoroBasic } from '../../providers/pomodoro-basic';

import { Task } from '../../models/task';
import {  PomodoroTask } from '../../providers/pomodoro-task';

import { TimerComponent } from './timer';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private tasks: Task[];
  private task: Task;
  private pomodoros: Pomodoro[];

  @ViewChild(TimerComponent) timer: TimerComponent;

  constructor(public navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private pomodoroBasic: PomodoroBasic,
              private pomodoroTask: PomodoroTask) {

  }

  ionViewDidLoad() {

    this.pomodoroTask
        .getTasks()
        .subscribe((tasks: Task[]) => {
          this.tasks = tasks;
        });

    this.pomodoroTask
        .getTask(1)
        .subscribe((task: Task) => {
          this.task = task;
        });

    this.pomodoroBasic
        .getPomodoros()
        .subscribe((pomodoros: Pomodoro[]) => {
            this.pomodoros = pomodoros;
        });
  }

  goToGraphsPage() {
    this.navCtrl.push(GraphsPage);
  }

  goToTeammembersPage() {
    this.navCtrl.push(TeammembersPage);
  }

  goToLeaderboardsPage() {
    this.navCtrl.push(LeaderboardsPage);
  }

  goToAccountPage() {
    this.navCtrl.push(AccountPage);
  }

  goToOptionsPage() {
    this.navCtrl.push(OptionsPage);
  }
}
