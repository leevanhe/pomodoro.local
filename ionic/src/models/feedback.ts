export interface Feedback {
    id?: number;
    title?: string;
    body?: string;
}
