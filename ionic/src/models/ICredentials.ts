export interface ICredentials  {
    _username: string;
    _password: string;
}