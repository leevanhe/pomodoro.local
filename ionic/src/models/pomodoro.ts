export class Pomodoro {
    id: number;
    time: number;
    shortBreak: number;
    longBreak: number;
}