import { BrowserModule } from '@angular/platform-browser';
import { HttpModule} from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { FormsModule }    from '@angular/forms';
import { StatusBar, Splashscreen } from 'ionic-native';
import { Storage } from '@ionic/storage';


import { StartupPage, LoginPage } from '../pages/startup/startup';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TimerComponent, PomodoroOptionsPage } from '../pages/home/timer';
import { SharePage }  from '../pages/share/share';
import { GraphsPage } from '../pages/graphs/graphs';
import { TeammembersPage, DetailPage } from '../pages/teammembers/teammembers';
import { LeaderboardsPage } from '../pages/leaderboards/leaderboards';
import { AccountPage } from '../pages/account/account';
import { AccountDetailPage } from '../pages/account-detail/account-detail';
import { OptionsPage } from '../pages/options/options';
import { SoundPage } from '../pages/sound/sound';
import { RatePage } from '../pages/rate/rate';
import { FeedbackPage } from '../pages/feedback/feedback';

import { PomodoroBasic } from '../providers/pomodoro-basic';
import { PomodoroTask } from '../providers/pomodoro-task';
import { PomodoroFeedback } from '../providers/pomodoro-feedback';
import { AuthService } from '../providers/pomodoro-auth.ts';

@NgModule({
  declarations: [
    MyApp,
    StartupPage,
    LoginPage,
    HomePage,
    PomodoroOptionsPage,
    TimerComponent,
    SharePage,
    GraphsPage,
    TeammembersPage,
    DetailPage,
    LeaderboardsPage,
    AccountPage,
    AccountDetailPage,
    OptionsPage,
    SoundPage,
    RatePage,
    FeedbackPage,
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    HttpModule,
    BrowserModule,
    FormsModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    StartupPage,
    LoginPage,
    HomePage,
    PomodoroOptionsPage,
    SharePage,
    GraphsPage,
    TeammembersPage,
    DetailPage,
    LeaderboardsPage,
    AccountPage,
    AccountDetailPage,
    OptionsPage,
    SoundPage,
    RatePage,
    FeedbackPage
  ],
  providers: [
      StatusBar,
      Splashscreen,
      [{provide: ErrorHandler, useClass: IonicErrorHandler}, Storage],
      PomodoroBasic,
      PomodoroTask,
      PomodoroFeedback,
      AuthService
  ]
})
export class AppModule {}
