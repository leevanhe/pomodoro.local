import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Events } from 'ionic-angular';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

import {User} from "../models/users";

@Injectable()
export class AuthService {
    currentUser: User;
    HAS_LOGGED_IN = 'hasLoggedIn';
    logStatusChecked: boolean = false;
    logStatus: boolean = false;
    private apiUrl: string = 'http://127.0.0.1:8000/api/';

    constructor(
        public events: Events,
        public storage: Storage,
        public http: Http
    ) {
    }

    public login(credentials) : any {
        if (credentials._username === null || credentials._password === null) {
            return Observable.throw("Please insert credentials");
        } else {
            console.log('Login Posted',credentials);
            return this.http.post(this.apiUrl+'login_check', credentials)
                .map(res => res.json())
                .subscribe(data =>
                    {
                        console.log(data);
                        let expireTime = new Date();
                        expireTime.setTime(expireTime.getTime() + (3600000));
                        localStorage.setItem('jwt_token', data.token);
                        let timestamp = {token: data.token, timestamp: expireTime.getTime()};
                        localStorage.setItem("login_token", JSON.stringify(timestamp));
                        this.logStatusChecked = false;
                        this.getUserInfo(data.data.id);
                        this.storage.set(this.HAS_LOGGED_IN, true);
                        this.events.publish('user:login');
                    },
                    error => console.log('Failure',error));
        }
    }

    public getUserInfo(id): Promise<any>{
        let self = this;

        let user = {
            name: null,
            data: null
        };
        let url: string = `${self.apiUrl}v1/users/${id}.json`,
            options: RequestOptions = new RequestOptions({headers: new Headers({'Authorization': 'Bearer ' + localStorage.getItem('jwt_token')})});

        return this.http
            .get(url, options)
            .toPromise()
            .then(function(res){
                let result = res.json();
                user.name = result.firstName + ' ' + result.lastName;
                user.data = result;
                localStorage.setItem("userData", JSON.stringify(user));
                console.log('Fetch data from current user', user);
                return self.currentUser;
            })
    }

    public fetchUser(){
        if (this.currentUser) {
            return this.currentUser;
        } else {
            let localUser = localStorage.getItem('userData');
            if (localUser) {
                this.currentUser = JSON.parse(localUser);
            } else {
                this.logout();
            }
        }
    }

    public logout() {
        this.currentUser = null;
        this.storage.remove(this.HAS_LOGGED_IN);
        this.logStatusChecked = false;
        localStorage.removeItem('jwt_token');
        localStorage.removeItem('login_token');
        localStorage.removeItem('userData');
        this.events.publish('user:logout');
    }

    public hasLoggedIn(){
        if(!this.logStatusChecked){
            let jwt = localStorage.getItem('jwt_token');
            let logT = JSON.parse(localStorage.getItem("login_token"));
            let status: boolean = false;
            if(logT && jwt){
                let now = new Date().getTime();
                if(jwt == logT.token){
                    if(logT.timestamp >= now ){
                        status = true;
                    }
                }
            }
            this.logStatus = status;
            this.logStatusChecked = true;
            if(!status){
                this.logout();
            }
            return status;
        } else {
            return this.logStatus;
        }

    }
}