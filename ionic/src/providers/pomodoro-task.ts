import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http'
import 'rxjs/add/operator/map';
import { Observable } from "rxjs";

import { Task } from '../models/task';

@Injectable()
export class PomodoroTask {

    private apiUrl: string = 'http://127.0.0.1:8000/api/v1/';

    constructor(public http: Http) {
        console.log('Hello Pomodorotask Provider');
    }

    getTasks(): Observable<Task[]> {
        let url: string = `${this.apiUrl}tasks.json`,
            options: RequestOptions = new RequestOptions({ headers: new Headers({ 'Authorization': 'Bearer ' + localStorage.getItem('jwt_token') }) });
        return this.http
            .get(url, options)
            .map((res: Response) => <Task[]>res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getTask(id: number) {
        let url: string = `${this.apiUrl}tasks/${id}.json`,
            options: RequestOptions = new RequestOptions({ headers: new Headers({ 'Authorization': 'Bearer ' + localStorage.getItem('jwt_token') }) });
        return this.http
            .get(url, options)
            .map((res: Response) => <Task>res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    postTask(task: Task) {
        let url: string = `${this.apiUrl}tasks/.json`,
            body: string = JSON.stringify({ task: task }),
            options: RequestOptions = new RequestOptions({ headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('jwt_token'),
                'Content-Type': 'application/json'
            }) });
        return this.http
            .post(url, body, options)
            .catch((error: any) => Observable.throw(error || 'Server error'))
            ;
    }

    putTask(task: Task) {
        let url: string = `${this.apiUrl}tasks/${task.id}.json`,
            body: string = JSON.stringify({ task: task }),
            options: RequestOptions = new RequestOptions({ headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('jwt_token'),
                'Content-Type': 'application/json'
            }) });
        return this.http
            .put(url, body, options)
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }


}
