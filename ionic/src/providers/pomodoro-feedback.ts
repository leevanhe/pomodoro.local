import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http'
import 'rxjs/add/operator/map';
import { Observable } from "rxjs";

import { Feedback } from '../models/feedback';

@Injectable()
export class PomodoroFeedback {

    private apiUrl: string = 'http://127.0.0.1:8000/api/v1/';

    constructor(public http: Http) {
        console.log('Hello Pomodorofeedback Provider');
    }

    postFeedback(feedback: Feedback) {
        let url: string = `${this.apiUrl}users/1/feedback/.json`,
            body: string = JSON.stringify({ feedback: feedback }),
            options: RequestOptions = new RequestOptions({ headers: new Headers({
                'Authorization': 'Bearer ' + localStorage.getItem('jwt_token'),
                'Content-Type': 'application/json'
            }) });
        return this.http
            .post(url, body, options)
            .catch((error: any) => Observable.throw(error || 'Server error'));
    }
}
