import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http'
import 'rxjs/add/operator/map';
import { Observable } from "rxjs";
import { Pomodoro } from '../models/pomodoro';

/*
  Generated class for the PomodoroBasic provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PomodoroBasic {

  private apiUrl: string = 'http://127.0.0.1:8000/api/v1/';

  constructor(public http: Http) {
    console.log('Hello PomodoroUsers Provider');
  }

  getPomodoros(): Observable<Pomodoro[]> {
    let url: string = `${this.apiUrl}pomodoros.json`,
        options: RequestOptions = new RequestOptions({ headers: new Headers({ 'Authorization': 'Bearer ' + localStorage.getItem('jwt_token') }) });

    return this.http
        .get(url, options)
        .map((res: Response) => <Pomodoro[]>res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
